#Dyzkus

An alternative to non-free Javascript used by Disqus

Now hosted at GitLab, which apparently helps advance human knowledge in ways that GitHub does not -- see [gnu.org/software/repo-criteria.html](http://www.gnu.org/software/repo-criteria.html)

TODO:

* Fix bug causing pictures in comments to appear multiple times after a page reload

* Figure out why two page reloads are necessary for deleted comments to disappear.

* Fix bug causing user avatar to display only on each user's last comment on [AstoundingTeam Guestbook](http://astoundingteam.com/guestbook).

* Figure out why user shortname comes up undefined on [IEEE Spectrum](http://spectrum.ieee.org/consumer-electronics/gadgets/for-texas-instruments-calculator-hackers-dont-add-up)

* Figure out what's happening re. avatar placement on [Stack Overflow Blog](https://blog.stackoverflow.com/2016/02/why-stack-overflow-doesnt-care-about-ad-blockers/).

* Figure out why mouse pointer doesn't go 'finger' for links.

* Special case, [Simon Thorpe's Ideas](http://simonthorpesideas.blogspot.com/2016/01/swapclear-533-trillion-in-transactions.html).  HTML before scripts contains no `#disqus_thread` element.  At some point, a script causes a `#disqus_thread` element to overwrite the innerHTML of a `.comments` thread, displacing code for Blogger comments.

* Figure out how to delete comments (in progress)

* Figure out how to include pictures in comments
