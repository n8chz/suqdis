function findShortname() {
 // probably a Drupal site:
 var val = $("script").text().match(/\bdisqus_shortname\s*=\s*['"](.+?)['"]/);
 if (val) {
  return val[1];
 }
 val = $("#disqus_thread");
 if (val.length) {
  val = val.html().match(/\/\/(.+?)\.disqus\.com/);
  if (val) {
   return val[1];
  }
 }
 val = $("div.disqus-noscript");
 if (val.length) {
  val = val.html().match(/\/\/(.+?)\.disqus\.com/);
  if (val) {
   return val[1];
  }
 }
 val = $("#dsq-count-scr"); // Disqus code for "plain vanilla" installations
 if (val.length) {
  val = val.attr("src").match(/\/\/(.+?)\.disqus\.com/);
  return val[1];
 }
 val = $("script").text().match(/['"]?disqusShortname['"]?\s*:\s*['"](.+?)['"]/);
 if (val) {
  return val[1];
 }
 alert("unknown method of identifying disqus_shortname,\nplease report URL to lori@astoundingteam.com");
 // One problematic URL is https://www.thefix.com/content/neurodiversity-autism-addiction-strengths-advocacy8555?page=all
}

$(function () {
  var disqusThreadElement = $("#disqus_thread");
  if (!disqusThreadElement.length) {
   var disqusInScript = $("script").text().match("disqus");
   if (disqusInScript) {
    disqusThreadElement = $(".comments");
   }
  }
  // If there is no #disqus_thread element, we assume this page is not set up
  // for Disqus.
  if (disqusThreadElement.length) {
   var disqusShortname = findShortname();
   // I have no idea how to calculate the value of version, so I cribbed it
   // from the Browser Console (with "Net" turned on).
   // var version = "8c6e89d0ebceae7d8179a2293c5b5660"; // copied from browser request
   // Assemble the comments URL from the pieces extracted so far:
   window.url = `http://disqus.com/embed/comments/?base=default&f=${disqusShortname}&t_u=${location.href}&s_o=default&version=${self.options.version}`;
   // Found this parameter at http://www.motherjones.com/kevin-drum/2016/02/2016-year-voters-finally-got-tired-reality
   var node = $("div.node-type-blogpost");
   if (node.length) {
    node = node.attr("id").replace("-", "/");
    if (typeof node != "undefined") {
     url += `&t_i=${node}`;
    }
   }
   // Why does encodeURI not escape '/' to '2F'?
   self.port.emit("gotURL", encodeURI(window.url).replace("node/", "node%2F"));
  }
  else {
   // Probably not a page containing Disqus comments
  }
});

