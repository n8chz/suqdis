// Show thread-level properties at top of #disqus-thread div, above replies.
self.port.on("gotThread", function (threadJSON) {
  var thread = JSON.parse(threadJSON);

  var commentThread = $("#disqus_thread");
  commentThread.data("threadId", thread.id);
  if (!commentThread.length) {
   commentThread = $(".comments");
  }
  commentThread.empty();

  var threadDiv = $("<div>");
  commentThread.prepend(threadDiv);
  threadDiv.attr("id", "thread");

  var convoSpan = $("<span>");
  convoSpan.addClass("tab-conversation");
  convoSpan.text(" Comments");
  var countSpan = $("<span>");
  countSpan.addClass("comment-count");
  countSpan.text(thread.posts);
  convoSpan.prepend(countSpan);

  var communityLink = $("<a>");
  communityLink.attr("id", "community-tab");
  communityLink.attr("href", `https://disqus.com/home/forums/${thread.forum}/`);
  communityLink.data("moderators", thread.moderators);
  var communitySpan = $("<span>");
  communitySpan.addClass("community-name");
  communitySpan.text(thread.identifiers[0]);
  communityLink.append(communitySpan);

  var commentButton = $("<button>");
  commentButton.addClass("reply");
  commentButton.addClass("thread");
  commentButton.text("Comment on article");
  commentButton.click(replyListener);

  threadDiv.append(commentButton).append("<br>").append(convoSpan).append(communityLink);
  self.port.emit("showedThread");
});
