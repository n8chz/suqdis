function deleteListener(postID) {
 return function () {
  var data = {
   post: postID,
   api_key: self.options.apiKey
  };
  self.port.emit("aboutToDelete", JSON.stringify(data));
 };
}


function cancelListener() {
 $("#suqdis-comment-form").remove();
}

// Listener function to attach to buttons for upvote and downvote.
function voteListener() {
 var data = {
    post: $(this).closest(".disqus-post").data("postId"),
    vote: $(this).hasClass("up") ? 1 : -1,
    api_key: self.options.apiKey
 };
 // The listener for "aboutToVote" is in lib/main.js
 self.port.emit("aboutToVote", JSON.stringify(data));
}


function commentListener(parentId) {
 return function () {
  var data = {
     thread: $("#disqus_thread").data("threadId"),
     message: $("#suqdis-comment-text").val(),
     "parent": parentId,
     api_key: self.options.apiKey
  };
  // The listener for "aboutToPost" is in lib/main.js
  self.port.emit("aboutToPost", JSON.stringify(data));
 };
}

// Listener function that can be attached to the "reply" button.  Creates a
// form with a textarea in which a reply can be written, and a button with
// which it can be submitted.  The click listener for that button is a function
// returned by commentListener.
function replyListener() {
 if ($("#suqdis-comment-form").length) {
  $("#suqdis-comment-text").focus();
 }
 else {
  if ($(this).hasClass("thread")) {
   var article = $("#disqus_thread");
   var articleID = undefined;
  }
  else {
   var article = $(this).closest("article");
   var articleID = article.closest(".disqus-post").data("postId");
  }
  var commentForm = $("<form>");
  commentForm.attr("id", "suqdis-comment-form");
  var thread = $("<input>");
  thread.attr("type", "hidden");
  thread.attr("name", "thread");
  thread.attr("value", articleID);
  var apiKey = $("<input>");
  apiKey.attr("type", "hidden");
  apiKey.attr("name", "api_key");
  apiKey.attr("value", self.options.apiKey);
  commentForm.append(thread).append(apiKey);
  var commentText = $("<textarea>");
  commentText.attr("id", "suqdis-comment-text");
  commentText.attr("name", "message");
  var submit = $("<button>");
  submit.attr("type", "button");
  submit.text("Comment");
  submit.click(commentListener(articleID));
  var cancel = $("<button>");
  cancel.text("Cancel");
  cancel.attr("type", "button");
  cancel.click(cancelListener);
  commentForm.append(commentText).append(cancel).append(submit);
  article.after(commentForm);
  commentText.focus();
 }
}

self.port.on("newComment", function (commentJSON) {
  var post = JSON.parse(commentJSON).response;
  // getCommentThread() seems to be redundant
  renderOnePost(post, getCommentThread()).focus();
  cancelListener();
});

function getPostByID(postID) {
 return $(".disqus-post").filter(function () {
   return $(this).data("postId") === postID.toString();
 });
}

// Update the numbers on the buttons to reflect the current number of upvotes
// and downvotes.
self.port.on("newVote", function (postJSON) {
  var post = JSON.parse(postJSON);
  var postElement = getPostByID(post.id);
  postElement.find(".up .vote-count").text(post.likes);
  postElement.find(".down .vote-count").text(post.dislikes);
});


self.port.on("newDelete", function (postID) {
  getPostByID(postID).remove();
});
