var pageMod = require("sdk/page-mod");
var Page = require("sdk/page-worker").Page;
var preferences = require("sdk/simple-prefs");
var Request = require("sdk/request").Request;

// console.error("checking in now");

Request({
  url: "https://disqus.com/profile/login/?next=%2F",
  onComplete: function (response) {
   // var token = response.headers.csrfmiddlewaretoken;
   var match = response.text.match(/name='csrfmiddlewaretoken' value='(.*)'/);
   if (match) { // If no match, assume we're already logged in on Disqus
    var token = match[1];
    // log in to disqus.com
    Request({
      url: "https://disqus.com/profile/login/?next=https://disqus.com/home",
      headers: {
       "Host": "disqus.com",
       "Origin": "https://disqus.com",
       // h/t http://www.thefakegeek.com/2015/01/how-to-fix-the-csrf-verification-failed-error-message/
       "Referer": "https://disqus.com/profile/login/?next=%2F",
       "Upgrade-Insecure-Requests": "1"
      },
      content: {
       csrfmiddlewaretoken: token,
       username: preferences.prefs["userName"],
       password: preferences.prefs["password"]
      },
      onComplete: function (response) {
       // console.error(`login response: ${JSON.stringify(response)}`);
      }
    }).post();
   }
   var params = {};
   params[`_${Date.now()}`] = 1;
   params.api_key = preferences.prefs["apiKey"];
   Request({
     url: "https://disqus.com/api/3.0/users/details.json",
     content: params,
     onComplete: function (response) {
      var user = JSON.parse(response.text).response;
      // These two values will go to showThread script
      userName = user.name;
      userID = user.id;
      mainPageMod(user.id, user.name);
     }
   }).get();
  }
}).get();

function post(worker, url) {
 return function (dataJSON) {
  Request({
    url: "https://disqus.com/api/3.0/posts/create.json",
    headers: {
     "Host": "disqus.com",
     "Origin": "https://disqus.com",
     // h/t http://www.thefakegeek.com/2015/01/how-to-fix-the-csrf-verification-failed-error-message/
     "Referer": url,
     "Upgrade-Insecure-Requests": "1"
    },
    content: JSON.parse(dataJSON),
    onComplete: function (response) {
     var post = JSON.parse(response.text);
     console.error(JSON.stringify(post));
     if (post.code) { // post.code is 0 for a successful request
      console.error(JSON.stringify(post));
     }
     else {
      worker.port.emit("newComment", response.text);
     }
    }
  }).post();
 };
}

function vote(worker, url) {
 return function (dataJSON) {
  Request({
    url: "https://disqus.com/api/3.0/posts/vote.json",
    headers: {
     "Host": "disqus.com",
     "Origin": "https://disqus.com",
     "Referer": url,
     "Upgrade-Insecure-Requests": "1"
    },
    content: JSON.parse(dataJSON),
    onComplete: function (response) {
     // result is found in response.post.likes, response.post.dislikes
     var post = JSON.parse(response.text).response.post;
     worker.port.emit("newVote", JSON.stringify(post));
    }
  }).post();
 };
}

function remove(worker, url) {
 return function (dataJSON) {
  params = JSON.parse(dataJSON);
  // console.error(`remove params: ${JSON.stringify(params)}`);
  Request({
    url: "https://disqus.com/api/3.0/posts/remove.json",
    headers: {
     "Host": "disqus.com",
     "Origin": "https://disqus.com",
     "Referer": url,
     "Upgrade-Insecure-Requests": "1"
    },
    content: params,
    onComplete: function (response) {
     // console.error(`remove.json response: ${JSON.stringify(response)}`);
     var postID = JSON.parse(response.text).response.id;
     worker.port.emit("newDelete", params.post);
    }
  }).post();
 };
}

function setupComments(url, worker, comments) {
 return function () {
   worker.port.emit("gotComments", JSON.stringify(comments));

   worker.port.on("aboutToVote", vote(worker, url));

   worker.port.on("aboutToPost", post(worker, url));

   worker.port.on("aboutToDelete", remove(worker, url));
 };
}

function getCommentStream(worker) {
 return function (url) {
  // Fetch Disqus comment stream URL and run content script
  // 'get-comments.js' on it to extractthe JSON-encoded comment object from
  // it.
  Page({
    contentURL: url,
    contentScriptFile: ["./jquery.js", "./get-comments.js"],
    // The JSON-serialized comment stream will come as a message from
    // get-comments.js.  This is then sent to 'show-comments.js' for
    // rendering.
    onMessage: function (threadDataJSON) {
     var response = JSON.parse(threadDataJSON).response;
     // Notifications count links to https://disqus.com/home/inbox/
     // console.error(`(getCommentStream) response: ${JSON.stringify(response)}`);
     worker.port.emit("gotThread", JSON.stringify(response.thread));
     var comments = response.posts;
     worker.port.on("showedThread", setupComments(url, worker, comments));
    }
  });
 };
}


function mainPageMod(userID, userName) {
 pageMod.PageMod({
   include: "*",
   contentScriptWhen: "end",
   contentScriptFile: [
    "./jquery.js",
    "./get-url.js",
    "./reply-listener.js",
    "./show-comments.js",
    "./show-thread.js"
   ],
   contentScriptOptions: {
    apiKey: preferences.prefs["apiKey"],
    version: preferences.prefs["version"],
    userID: userID,
    userName: userName
   },
   contentStyleFile: ["./dyzkus.css"],
   onAttach: function (worker) {
    // "gotURL" event is emitted by data/get-url.js whenever a page is visited
    // that has an element with id="disqus_thread".  The payload from that event
    // is a URL which contains the comment stream encoded in JSON.
    worker.port.on("gotURL", getCommentStream(worker));
   }
 });
}

